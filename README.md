# Flectra Community / fleet

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[fleet_vehicle_service_services](fleet_vehicle_service_services/) | 2.0.1.0.0| Add subservices in Services.
[fleet_vehicle_service_calendar](fleet_vehicle_service_calendar/) | 2.0.1.0.0| Add a smart button in services to schedule meetings.
[fleet_vehicle_service_kanban](fleet_vehicle_service_kanban/) | 2.0.1.0.0| Add features of kanban to logs of vehicle services.
[fleet_vehicle_category](fleet_vehicle_category/) | 2.0.1.1.0| Add category definition for vehicles.
[fleet_vehicle_fuel_capacity](fleet_vehicle_fuel_capacity/) | 2.0.1.0.1|         This module extends the functionality of fleet management. It allows the        registration of a vehicle's fuel capacity.
[fleet_vehicle_log_fuel](fleet_vehicle_log_fuel/) | 2.0.1.0.0| Add Log Fuels for your vehicles.
[fleet_vehicle_calendar_year](fleet_vehicle_calendar_year/) | 2.0.1.0.1|         This module extends the fleet management functionality. Allows the        registration of the vehicle's calendar year.
[fleet_vehicle_inspection](fleet_vehicle_inspection/) | 2.0.3.0.1|         This module extends the Fleet module allowing the registration        of vehicle entry and exit inspections.
[fleet_vehicle_pivot_graph](fleet_vehicle_pivot_graph/) | 2.0.1.0.0|         This module extends the fleet management functionality. Adds the pivot        table and graph view to the fleet vehicles.
[fleet_vehicle_inspection_template](fleet_vehicle_inspection_template/) | 2.0.1.0.0|         This module extend module fleet_vehicle_inspection enable        inspection templates feature
[fleet_vehicle_fuel_type_ethanol](fleet_vehicle_fuel_type_ethanol/) | 2.0.1.0.0|         This module extends the fleet management functionality. This adds ethanol        as another type of fuel to be used by a vehicle in the fleet.
[fleet_vehicle_notebook](fleet_vehicle_notebook/) | 2.0.1.0.0|         This module provides an empty notebook for the vehicle form.
[fleet_vehicle_history_date_end](fleet_vehicle_history_date_end/) | 2.0.1.0.0| Automatically assign date end in vehicle history when a new driver is assigned.



# Copyright 2021 - TODAY, Escflectra
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Fleet Vehicle Fuel Type Ethanol",
    "summary": """
        This module extends the fleet management functionality. This adds ethanol
        as another type of fuel to be used by a vehicle in the fleet.""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "category": "Human Resources/Fleet",
    "author": "Escflectra,Odoo Community Association (OCA)",
    "maintainers": ["marcelsavegnago"],
    "images": ["static/description/banner.png"],
    "website": "https://gitlab.com/flectra-community/fleet",
    "depends": [
        "fleet",
    ],
}

# Copyright 2020 Stefano Consolaro (Ass. PNLUG - Gruppo Odoo <http://flectra.pnlug.it>)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Fleet Vehicle Category",
    "summary": "Add category definition for vehicles.",
    "version": "2.0.1.1.0",
    "category": "Human Resources",
    "author": "Stefano Consolaro, Associazione PNLUG - Gruppo Odoo, "
    "Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/fleet",
    "license": "AGPL-3",
    "depends": ["fleet"],
    "data": ["security/ir.model.access.csv", "views/fleet_vehicle_views.xml"],
    "installable": True,
    "auto_install": False,
}
